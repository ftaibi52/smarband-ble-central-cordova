
var app_voz = {

    prueba(){
        document.addEventListener('deviceready', function () {
            // basic usage
            TTS
                .speak('hello, world!').then(function () {
                    alert('success');
                }, function (reason) {
                    alert(reason);
                });
        
            // or with more options
            TTS
                .speak({
                    text: 'hello, world!',
                    locale: 'en-GB',
                    rate: 0.75
                }).then(function () {
                    alert('success');
                }, function (reason) {
                    alert(reason);
                });
        }, false);
        
        
    },
}

