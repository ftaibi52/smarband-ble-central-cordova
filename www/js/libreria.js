const UUID_service = 'be940000-7333-be46-b7ae-689e71722bd5';
const UUID = 'be940001-7333-be46-b7ae-689e71722bd5';
const UUID3 = 'be940003-7333-be46-b7ae-689e71722bd5';
var heartRate = {
	service: '180d',
	measurement: '2a37'
};

var device;

var ictus = {


	/**
	 * 
	 * @param {*} listarDispositivos 
	 * Estas funcion recibe como parametro una funcion que lista los dispositivos obtenidos
	 * La funcion busca durante 15 s las pulseras más cercanas, obtiene id, nombre y rrssi de la pulsera
	 */
	buscarpulseras(listarDispositivos) {

		ble.enable(onSucces, this.onFailure);

		function onSucces() {

			console.log('BUSCANDO...');

			// buscará solo dispositivos que tienen el servicio de heartRate
			ble.scan([heartRate.service], 15, listadoPulseras, this.onFailure);

			function listadoPulseras(pulseras) {
				console.log(pulseras.id);
				listarDispositivos(pulseras);

			};
		}

	},

	conectar(deviceId) {

		ble.connect(deviceId, onConnect, this.onFailure);

		function onConnect() {

			device = deviceId;

			console.log("Connected to " + device);

			ble.stopScan(function () {

				console.log("Stop Scan Devices");

			},
				ictus.onFailure);
		}
	},

	desconectar() {
		ble.disconnect(device, function () { console.log("Desconectado"); }, this.onFailure);
	},

	/**
	 * 
	 * @param {*} funcion este parámetro es un función 
	 * 
	 * En esta funcion se miden tres datos sistolica, diastolica y pulsaciones que se almacenan en un array 
	 *  
	 */
	medirSalud(funcion) {

		//Enviando array para iniciar la medicion en la pulsera...
		var data = new Uint8Array([3, 2, 7, 0, 2, 100, -73]);

		ble.writeWithoutResponse(device, UUID_service, UUID, data.buffer, this.onSucces, this.onFailure);
		console.log("Leyendo datos de salud...");

		var data = new Uint8Array([3, 9, 9, 0, 1, 3, 5, 20, -5]);

		ble.writeWithoutResponse(device, UUID_service, UUID, data.buffer, this.onSucces, this.onFailure);

		//starNotificacion para actualizar los datos cada vez que cambian (tener en cuenta en UUID char es el tercero 'be940003-7333-be46-b7ae-689e71722bd5')

		//externalFunction("Hasta luego");

		ble.startNotification(device, UUID_service, UUID3, function (e) {

			var data = new Uint8Array(e);

			var arraySalud = {
				'sistolica': data[4],
				'distolica': data[5],
				'pulsaciones': data[6]
			};

			funcion(arraySalud);

		}, this.onFailure);
	},

	/**
 * 
 * @param {*} funcion este parámetro es un función 
 * 
 * En esta funcion se miden tres datos sistolica, diastolica y pulsaciones que se almacenan en un array 
 *  
 */
	medirSalud(funcion) {

		//Enviando array para iniciar la medicion en la pulsera...
		var data = new Uint8Array([3, 2, 7, 0, 2, 100, -73]);

		ble.writeWithoutResponse(device, UUID_service, UUID, data.buffer, this.onSucces, this.onFailure);
		console.log("Leyendo datos de salud...");

		var data = new Uint8Array([3, 9, 9, 0, 1, 3, 5, 20, -5]);

		ble.writeWithoutResponse(device, UUID_service, UUID, data.buffer, this.onSucces, this.onFailure);

		//starNotificacion para actualizar los datos cada vez que cambian (tener en cuenta en UUID char es el tercero 'be940003-7333-be46-b7ae-689e71722bd5')

		ble.startNotification(device, UUID_service, UUID3, function (e) {

			var data = new Uint8Array(e);

			var systolic = data[4];
			var diastolic = data[5];
			var beatsPerMinute = data[6];

			var arraySalud = [systolic, diastolic, beatsPerMinute];
			console.log("Array salud: " + arraySalud);

			funcion(arraySalud);

		}, this.onFailure);


	},
	medirDeporte(funcion) {

		console.log("Leyendo datos de deporte...");

		//se envia el primer array para abrir canal de comunicacion 
		var data = new Uint8Array([3, 2, 7, 0, 0, 38, -105]);
		ble.writeWithoutResponse(device, UUID_service, UUID, data.buffer, this.onSucces, this.onFailure);

		//se envia el segundo array para empezar a leer y recibir los datos
		var data2 = new Uint8Array([3, 9, 9, 0, 1, 0, 3, -127, -50]);
		ble.writeWithoutResponse(device, UUID_service, UUID, data2.buffer, this.onSucces, this.onFailure);

		//cada vez que cambien los datos seran enviados 
		ble.startNotification(device, UUID_service, UUID3, function (e) {

			//se pasa el array de bytes a Hex y se envia a la vista
			var data = new Uint16Array(e);

			var arrayDeporte = {
				'pasos': data[2],
				'km': data[3] / 1000,
				'kcal': data[4]
			};

			funcion(arrayDeporte);

		}, this.onFailure);

	},

	pararMedicion() {

		var data = new Uint8Array([3, 2, 7, 0, 0, 38, -105]);

		ble.write(device, UUID_service, UUID, data.buffer, this.onSucces, this.onFailure);
		ble.stopNotification(device, UUID_service, UUID3);
	},

	onSucces(e) {
		console.log(e);
	},

	onFailure(err) {
		console.log('Error: '+ err);

	}
}

